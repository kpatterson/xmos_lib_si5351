/**************************************************************************/
/*!
    @file     Si5351.xc
    @author   Kevin H. Patterson (kpatterson _at_ creltek _dot_ com)
    @author   based on original code by K. Townsend (Adafruit Industries)

    @brief    Driver for the Si5351 160MHz Clock Gen

    @section  REFERENCES

    Si5351A/B/C Datasheet:
    http://www.silabs.com/Support%20Documents/TechnicalDocs/Si5351.pdf

    Manually Generating an Si5351 Register Map:
    http://www.silabs.com/Support%20Documents/TechnicalDocs/AN619.pdf

    @section  LICENSE

    Software License Agreement (BSD License)

    Copyright (C) 2017, Kevin H. Patterson
    Copyright (C) 2014, Adafruit Industries
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holders nor the
    names of its contributors may be used to endorse or promote products
    derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    ( INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION ) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    ( INCLUDING NEGLIGENCE OR OTHERWISE ) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/**************************************************************************/

#include <Si5351.xc.h>
#include <math.h>

#include "Si5351_asserts.h"


typedef enum {
  none = 0,
  initialised = 0b0001,
  plla_configured = 0b0010,
  pllb_configured = 0b0100
} Si5351ConfigFlags_t;

typedef struct {
//    uint32_t    crystalPPM;
    uint32_t    plla_freq;
    uint32_t    pllb_freq;
    uint32_t    crystalFreq;    // Si5351CrystalFreq_t
    uint8_t     crystalLoad;    // Si5351CrystalLoad_t
    uint8_t     Flags;
    uint8_t     i2cAddr;
} Si5351Config_t;


/* Test setup from Si5351 ClockBuilder
 * -----------------------------------
 * XTAL:      25     MHz
 * Channel 0: 120.00 MHz
 * Channel 1: 12.00  MHz
 * Channel 2: 13.56  MHz
 */
static const uint8_t m_Si5351_regs_15to92_149to170[100][2] =
{
  {  15, 0x00 },    /* Input source = crystal for PLLA and PLLB */
  {  16, 0x4F },    /* CLK0 Control: 8mA drive, Multisynth 0 as CLK0 source, Clock not inverted, Source = PLLA, Multisynth 0 in integer mode, clock powered up */
  {  17, 0x4F },    /* CLK1 Control: 8mA drive, Multisynth 1 as CLK1 source, Clock not inverted, Source = PLLA, Multisynth 1 in integer mode, clock powered up */
  {  18, 0x6F },    /* CLK2 Control: 8mA drive, Multisynth 2 as CLK2 source, Clock not inverted, Source = PLLB, Multisynth 2 in integer mode, clock powered up */
  {  19, 0x80 },    /* CLK3 Control: Not used ... clock powered down */
  {  20, 0x80 },    /* CLK4 Control: Not used ... clock powered down */
  {  21, 0x80 },    /* CLK5 Control: Not used ... clock powered down */
  {  22, 0x80 },    /* CLK6 Control: Not used ... clock powered down */
  {  23, 0x80 },    /* CLK7 Control: Not used ... clock powered down */
  {  24, 0x00 },    /* Clock disable state 0..3 (low when disabled) */
  {  25, 0x00 },    /* Clock disable state 4..7 (low when disabled) */
  /* PLL_A Setup */
  {  26, 0x00 },
  {  27, 0x05 },
  {  28, 0x00 },
  {  29, 0x0C },
  {  30, 0x66 },
  {  31, 0x00 },
  {  32, 0x00 },
  {  33, 0x02 },
  /* PLL_B Setup */
  {  34, 0x02 },
  {  35, 0x71 },
  {  36, 0x00 },
  {  37, 0x0C },
  {  38, 0x1A },
  {  39, 0x00 },
  {  40, 0x00 },
  {  41, 0x86 },
  /* Multisynth Setup */
  {  42, 0x00 },
  {  43, 0x01 },
  {  44, 0x00 },
  {  45, 0x01 },
  {  46, 0x00 },
  {  47, 0x00 },
  {  48, 0x00 },
  {  49, 0x00 },
  {  50, 0x00 },
  {  51, 0x01 },
  {  52, 0x00 },
  {  53, 0x1C },
  {  54, 0x00 },
  {  55, 0x00 },
  {  56, 0x00 },
  {  57, 0x00 },
  {  58, 0x00 },
  {  59, 0x01 },
  {  60, 0x00 },
  {  61, 0x18 },
  {  62, 0x00 },
  {  63, 0x00 },
  {  64, 0x00 },
  {  65, 0x00 },
  {  66, 0x00 },
  {  67, 0x00 },
  {  68, 0x00 },
  {  69, 0x00 },
  {  70, 0x00 },
  {  71, 0x00 },
  {  72, 0x00 },
  {  73, 0x00 },
  {  74, 0x00 },
  {  75, 0x00 },
  {  76, 0x00 },
  {  77, 0x00 },
  {  78, 0x00 },
  {  79, 0x00 },
  {  80, 0x00 },
  {  81, 0x00 },
  {  82, 0x00 },
  {  83, 0x00 },
  {  84, 0x00 },
  {  85, 0x00 },
  {  86, 0x00 },
  {  87, 0x00 },
  {  88, 0x00 },
  {  89, 0x00 },
  {  90, 0x00 },
  {  91, 0x00 },
  {  92, 0x00 },
  /* Misc Config Register */
  { 149, 0x00 },
  { 150, 0x00 },
  { 151, 0x00 },
  { 152, 0x00 },
  { 153, 0x00 },
  { 154, 0x00 },
  { 155, 0x00 },
  { 156, 0x00 },
  { 157, 0x00 },
  { 158, 0x00 },
  { 159, 0x00 },
  { 160, 0x00 },
  { 161, 0x00 },
  { 162, 0x00 },
  { 163, 0x00 },
  { 164, 0x00 },
  { 165, 0x00 },
  { 166, 0x00 },
  { 167, 0x00 },
  { 168, 0x00 },
  { 169, 0x00 },
  { 170, 0x00 }
};

/* See http://www.silabs.com/Support%20Documents/TechnicalDocs/AN619.pdf for registers 26..41 */
enum
{
  Si5351_REGISTER_0_STATUS                       = 0,
  Si5351_REGISTER_1_INTERRUPT_STATUS_STICKY             = 1,
  Si5351_REGISTER_2_INTERRUPT_STATUS_MASK               = 2,
  Si5351_REGISTER_3_OUTPUT_ENABLE_CONTROL               = 3,
  Si5351_REGISTER_9_OEB_PIN_ENABLE_CONTROL              = 9,
  Si5351_REGISTER_15_PLL_INPUT_SOURCE                   = 15,
  Si5351_REGISTER_16_CLK0_CONTROL                       = 16,
  Si5351_REGISTER_17_CLK1_CONTROL                       = 17,
  Si5351_REGISTER_18_CLK2_CONTROL                       = 18,
  Si5351_REGISTER_19_CLK3_CONTROL                       = 19,
  Si5351_REGISTER_20_CLK4_CONTROL                       = 20,
  Si5351_REGISTER_21_CLK5_CONTROL                       = 21,
  Si5351_REGISTER_22_CLK6_CONTROL                       = 22,
  Si5351_REGISTER_23_CLK7_CONTROL                       = 23,
  Si5351_REGISTER_24_CLK3_0_DISABLE_STATE               = 24,
  Si5351_REGISTER_25_CLK7_4_DISABLE_STATE               = 25,
  Si5351_REGISTER_42_MULTISYNTH0_PARAMETERS_1           = 42,
  Si5351_REGISTER_43_MULTISYNTH0_PARAMETERS_2           = 43,
  Si5351_REGISTER_44_MULTISYNTH0_PARAMETERS_3           = 44,
  Si5351_REGISTER_45_MULTISYNTH0_PARAMETERS_4           = 45,
  Si5351_REGISTER_46_MULTISYNTH0_PARAMETERS_5           = 46,
  Si5351_REGISTER_47_MULTISYNTH0_PARAMETERS_6           = 47,
  Si5351_REGISTER_48_MULTISYNTH0_PARAMETERS_7           = 48,
  Si5351_REGISTER_49_MULTISYNTH0_PARAMETERS_8           = 49,
  Si5351_REGISTER_50_MULTISYNTH1_PARAMETERS_1           = 50,
  Si5351_REGISTER_51_MULTISYNTH1_PARAMETERS_2           = 51,
  Si5351_REGISTER_52_MULTISYNTH1_PARAMETERS_3           = 52,
  Si5351_REGISTER_53_MULTISYNTH1_PARAMETERS_4           = 53,
  Si5351_REGISTER_54_MULTISYNTH1_PARAMETERS_5           = 54,
  Si5351_REGISTER_55_MULTISYNTH1_PARAMETERS_6           = 55,
  Si5351_REGISTER_56_MULTISYNTH1_PARAMETERS_7           = 56,
  Si5351_REGISTER_57_MULTISYNTH1_PARAMETERS_8           = 57,
  Si5351_REGISTER_58_MULTISYNTH2_PARAMETERS_1           = 58,
  Si5351_REGISTER_59_MULTISYNTH2_PARAMETERS_2           = 59,
  Si5351_REGISTER_60_MULTISYNTH2_PARAMETERS_3           = 60,
  Si5351_REGISTER_61_MULTISYNTH2_PARAMETERS_4           = 61,
  Si5351_REGISTER_62_MULTISYNTH2_PARAMETERS_5           = 62,
  Si5351_REGISTER_63_MULTISYNTH2_PARAMETERS_6           = 63,
  Si5351_REGISTER_64_MULTISYNTH2_PARAMETERS_7           = 64,
  Si5351_REGISTER_65_MULTISYNTH2_PARAMETERS_8           = 65,
  Si5351_REGISTER_66_MULTISYNTH3_PARAMETERS_1           = 66,
  Si5351_REGISTER_67_MULTISYNTH3_PARAMETERS_2           = 67,
  Si5351_REGISTER_68_MULTISYNTH3_PARAMETERS_3           = 68,
  Si5351_REGISTER_69_MULTISYNTH3_PARAMETERS_4           = 69,
  Si5351_REGISTER_70_MULTISYNTH3_PARAMETERS_5           = 70,
  Si5351_REGISTER_71_MULTISYNTH3_PARAMETERS_6           = 71,
  Si5351_REGISTER_72_MULTISYNTH3_PARAMETERS_7           = 72,
  Si5351_REGISTER_73_MULTISYNTH3_PARAMETERS_8           = 73,
  Si5351_REGISTER_74_MULTISYNTH4_PARAMETERS_1           = 74,
  Si5351_REGISTER_75_MULTISYNTH4_PARAMETERS_2           = 75,
  Si5351_REGISTER_76_MULTISYNTH4_PARAMETERS_3           = 76,
  Si5351_REGISTER_77_MULTISYNTH4_PARAMETERS_4           = 77,
  Si5351_REGISTER_78_MULTISYNTH4_PARAMETERS_5           = 78,
  Si5351_REGISTER_79_MULTISYNTH4_PARAMETERS_6           = 79,
  Si5351_REGISTER_80_MULTISYNTH4_PARAMETERS_7           = 80,
  Si5351_REGISTER_81_MULTISYNTH4_PARAMETERS_8           = 81,
  Si5351_REGISTER_82_MULTISYNTH5_PARAMETERS_1           = 82,
  Si5351_REGISTER_83_MULTISYNTH5_PARAMETERS_2           = 83,
  Si5351_REGISTER_84_MULTISYNTH5_PARAMETERS_3           = 84,
  Si5351_REGISTER_85_MULTISYNTH5_PARAMETERS_4           = 85,
  Si5351_REGISTER_86_MULTISYNTH5_PARAMETERS_5           = 86,
  Si5351_REGISTER_87_MULTISYNTH5_PARAMETERS_6           = 87,
  Si5351_REGISTER_88_MULTISYNTH5_PARAMETERS_7           = 88,
  Si5351_REGISTER_89_MULTISYNTH5_PARAMETERS_8           = 89,
  Si5351_REGISTER_90_MULTISYNTH6_PARAMETERS             = 90,
  Si5351_REGISTER_91_MULTISYNTH7_PARAMETERS             = 91,
  Si5351_REGISTER_092_CLOCK_6_7_OUTPUT_DIVIDER          = 92,
  Si5351_REGISTER_165_CLK0_INITIAL_PHASE_OFFSET         = 165,
  Si5351_REGISTER_166_CLK1_INITIAL_PHASE_OFFSET         = 166,
  Si5351_REGISTER_167_CLK2_INITIAL_PHASE_OFFSET         = 167,
  Si5351_REGISTER_168_CLK3_INITIAL_PHASE_OFFSET         = 168,
  Si5351_REGISTER_169_CLK4_INITIAL_PHASE_OFFSET         = 169,
  Si5351_REGISTER_170_CLK5_INITIAL_PHASE_OFFSET         = 170,
  Si5351_REGISTER_177_PLL_RESET                         = 177,
  Si5351_REGISTER_183_CRYSTAL_INTERNAL_LOAD_CAPACITANCE = 183
};

/* ---------------------------------------------------------------------- */
/* PRIVATE FUNCTIONS                                                      */
/* ---------------------------------------------------------------------- */

/**************************************************************************/
/*!
    @brief  Writes an 8 bit value to a register over I2C
*/
/**************************************************************************/
err_t write8( Si5351Config_t* Config, client i2c_master_if i2c, uint8_t reg, uint8_t value ) {
    i2c_regop_res_t t_Success = i2c.write_reg( Config->i2cAddr, reg, value );
    switch( t_Success ) {
        case I2C_REGOP_SUCCESS:
            return ERROR_NONE;
        case I2C_REGOP_DEVICE_NACK:
            return ERROR_I2C_DEVICENOTFOUND;
        case I2C_REGOP_INCOMPLETE:
            return ERROR_I2C_NOACK;
    }
    return ERROR_UNEXPECTEDVALUE;
}

/**************************************************************************/
/*!
    @brief  Reads an 8 bit value over I2C
*/
/**************************************************************************/
err_t read8( Si5351Config_t* Config, client i2c_master_if i2c, uint8_t reg, uint8_t* value ) {
    i2c_regop_res_t t_Success;
    *value = i2c.read_reg( Config->i2cAddr, reg, t_Success );
    switch( t_Success ) {
        case I2C_REGOP_SUCCESS:
            return ERROR_NONE;
        case I2C_REGOP_DEVICE_NACK:
            return ERROR_I2C_DEVICENOTFOUND;
        case I2C_REGOP_INCOMPLETE:
            return ERROR_I2C_NOACK;
    }
    return ERROR_UNEXPECTEDVALUE;
}

/**************************************************************************/
/*!
    Constructor
*/
/**************************************************************************/
void Si5351_construct( Si5351Config_t* Config, uint8_t i2cAddr ) {
    Config->i2cAddr = i2cAddr;
    Config->Flags = none;
    Config->crystalFreq     = Si5351_CRYSTAL_FREQ_25MHZ;
    Config->crystalLoad     = Si5351_CRYSTAL_LOAD_10PF;
//    Config->crystalPPM      = 30;
    Config->plla_freq       = 0;
    Config->pllb_freq       = 0;
}

/**************************************************************************/
/*!
    Initializes I2C and configures the breakout ( call this function before
    doing anything else )
*/
/**************************************************************************/
err_t Si5351_init( Si5351Config_t* Config, client i2c_master_if i2c ) {
  /* ToDo: Make sure we're actually connected */

  /* Disable all outputs setting CLKx_DIS high */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_3_OUTPUT_ENABLE_CONTROL, 0xFF ) );

  /* Power down all output drivers */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_16_CLK0_CONTROL, 0x80 ) );
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_17_CLK1_CONTROL, 0x80 ) );
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_18_CLK2_CONTROL, 0x80 ) );
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_19_CLK3_CONTROL, 0x80 ) );
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_20_CLK4_CONTROL, 0x80 ) );
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_21_CLK5_CONTROL, 0x80 ) );
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_22_CLK6_CONTROL, 0x80 ) );
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_23_CLK7_CONTROL, 0x80 ) );

  /* Set the load capacitance for the XTAL */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_183_CRYSTAL_INTERNAL_LOAD_CAPACITANCE,
                       Config->crystalLoad ) );

  /* Set interrupt masks as required ( see Register 2 description in AN619 ).
     By default, ClockBuilder Desktop sets this register to 0x18.
     Note that the least significant nibble must remain 0x8, but the most
     significant nibble may be modified to suit your needs. */

  /* Reset the PLL config fields just in case we call init again */
  Config->plla_freq = 0;
  Config->pllb_freq = 0;

  /* All done! */
  Config->Flags = initialised;

  return ERROR_NONE;
}

/**************************************************************************/
/*!
    @brief  Configures the Si5351 with config settings generated in
            ClockBuilder. You can use this function to make sure that
            your HW is properly configured and that there are no problems
            with the board itself.

            Running this function should provide the following output:
            * Channel 0: 120.00 MHz
            * Channel 1: 12.00  MHz
            * Channel 2: 13.56  MHz

    @note   This will overwrite all of the config registers!
*/
/**************************************************************************/
err_t Si5351_setClockBuilderData( Si5351Config_t* Config, client i2c_master_if i2c ) {
  uint16_t i = 0;

  /* Make sure we've called init first */
  ASSERT( (Config->Flags & initialised), ERROR_DEVICENOTINITIALISED );

  /* Disable all outputs setting CLKx_DIS high */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_3_OUTPUT_ENABLE_CONTROL, 0xFF ) );

  /* Writes configuration data to device using the register map contents
	 generated by ClockBuilder Desktop ( registers 15-92 + 149-170 ) */
  for ( i=0; i<sizeof( m_Si5351_regs_15to92_149to170 )/2; i++ )
  {
    ASSERT_STATUS( write8( Config, i2c, m_Si5351_regs_15to92_149to170[i][0],
                          m_Si5351_regs_15to92_149to170[i][1] ) );
  }

  /* Apply soft reset */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_177_PLL_RESET, 0xAC ) );

  /* Enabled desired outputs ( see Register 3 ) */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_3_OUTPUT_ENABLE_CONTROL, 0x00 ) );

  return ERROR_NONE;
}

/**************************************************************************/
/*!
  @brief  Sets the multiplier for the specified PLL using integer values

  @param  pll   The PLL to configure, which must be one of the following:
                - Si5351_PLL_A
                - Si5351_PLL_B
  @param  mult  The PLL integer multiplier ( must be between 15 and 90 )
*/
/**************************************************************************/


/**************************************************************************/
/*!
    @brief  Sets the multiplier for the specified PLL

    @param  pll   The PLL to configure, which must be one of the following:
                  - Si5351_PLL_A
                  - Si5351_PLL_B
    @param  mult  The PLL integer multiplier ( must be between 15 and 90 )
    @param  num   The 20-bit numerator for fractional output ( 0..1,048,575 ).
                  Set this to '0' for integer output.
    @param  denom The 20-bit denominator for fractional output ( 1..1,048,575 ).
                  Set this to '1' or higher to avoid divider by zero errors.

    @section PLL Configuration

    fVCO is the PLL output, and must be between 600..900MHz, where:

        fVCO = fXTAL * ( a+( b/c ) )

    fXTAL = the crystal input frequency
    a     = an integer between 15 and 90
    b     = the fractional numerator ( 0..1,048,575 )
    c     = the fractional denominator ( 1..1,048,575 )

    NOTE: Try to use integers whenever possible to avoid clock jitter
    ( only use the a part, setting b to '0' and c to '1' ).

    See: http://www.silabs.com/Support%20Documents/TechnicalDocs/AN619.pdf
*/
/**************************************************************************/
err_t Si5351_setupPLL( Si5351Config_t* Config, client i2c_master_if i2c,
        Si5351PLL_t pll,
        uint8_t     mult,
        uint32_t    num,
        uint32_t    denom )
{
  uint32_t P1;       /* PLL config register P1 */
  uint32_t P2;	     /* PLL config register P2 */
  uint32_t P3;	     /* PLL config register P3 */

  /* Basic validation */
  ASSERT( (Config->Flags & initialised), ERROR_DEVICENOTINITIALISED );
  ASSERT( ( mult > 14 ) && ( mult < 91 ), ERROR_INVALIDPARAMETER ); /* mult = 15..90 */
  ASSERT( denom > 0,                  ERROR_INVALIDPARAMETER ); /* Avoid divide by zero */
  ASSERT( num <= 0xFFFFF,             ERROR_INVALIDPARAMETER ); /* 20-bit limit */
  ASSERT( denom <= 0xFFFFF,           ERROR_INVALIDPARAMETER ); /* 20-bit limit */

  /* Feedback Multisynth Divider Equation
   *
   * where: a = mult, b = num and c = denom
   *
   * P1 register is an 18-bit value using following formula:
   *
   * 	P1[17:0] = 128 * mult + floor( 128*( num/denom ) ) - 512
   *
   * P2 register is a 20-bit value using the following formula:
   *
   * 	P2[19:0] = 128 * num - denom * floor( 128*( num/denom ) )
   *
   * P3 register is a 20-bit value using the following formula:
   *
   * 	P3[19:0] = denom
   */

  /* Set the main PLL config registers */
  if ( num == 0 )
  {
    /* Integer mode */
    P1 = 128 * mult - 512;
    P2 = num;
    P3 = denom;
  }
  else
  {
    /* Fractional mode */
    P1 = ( uint32_t )( 128 * mult + floor( 128 * ( ( float )num/( float )denom ) ) - 512 );
    P2 = ( uint32_t )( 128 * num - denom * floor( 128 * ( ( float )num/( float )denom ) ) );
    P3 = denom;
  }

  /* Get the appropriate starting point for the PLL registers */
  uint8_t baseaddr = ( pll == Si5351_PLL_A ? 26 : 34 );

  /* The datasheet is a nightmare of typos and inconsistencies here! */
  ASSERT_STATUS( write8( Config, i2c, baseaddr,   ( P3 & 0x0000FF00 ) >> 8 ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+1, ( P3 & 0x000000FF ) ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+2, ( P1 & 0x00030000 ) >> 16 ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+3, ( P1 & 0x0000FF00 ) >> 8 ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+4, ( P1 & 0x000000FF ) ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+5, ( ( P3 & 0x000F0000 ) >> 12 ) | ( ( P2 & 0x000F0000 ) >> 16 ) ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+6, ( P2 & 0x0000FF00 ) >> 8 ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+7, ( P2 & 0x000000FF ) ) );

  /* Reset both PLLs */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_177_PLL_RESET, ( 1<<7 ) | ( 1<<5 ) ) );

  /* Store the frequency settings for use with the Multisynth helper */
  if ( pll == Si5351_PLL_A )
  {
    float fvco = Config->crystalFreq * ( mult + ( ( float )num / ( float )denom ) );
    Config->Flags |= plla_configured;
    Config->plla_freq = ( uint32_t )floor( fvco );
  }
  else
  {
    float fvco = Config->crystalFreq * ( mult + ( ( float )num / ( float )denom ) );
    Config->Flags |= pllb_configured;
    Config->pllb_freq = ( uint32_t )floor( fvco );
  }

  return ERROR_NONE;
}

/**************************************************************************/
/*!
    @brief  Configures the Multisynth divider using integer output.

    @param  output    The output channel to use ( 0..2 )
    @param  pllSource	The PLL input source to use, which must be one of:
                      - Si5351_PLL_A
                      - Si5351_PLL_B
    @param  div       The integer divider for the Multisynth output,
                      which must be one of the following values:
                      - Si5351_MULTISYNTH_DIV_4
                      - Si5351_MULTISYNTH_DIV_6
                      - Si5351_MULTISYNTH_DIV_8
*/
/**************************************************************************/


err_t Si5351_setupRdiv( Si5351Config_t* Config, client i2c_master_if i2c, uint8_t output, Si5351RDiv_t div ) {
  ASSERT( output < 3, ERROR_INVALIDPARAMETER );  /* Channel range */
  
  uint8_t Rreg, regval;

  switch( output ) {
  case 0:
      Rreg = Si5351_REGISTER_44_MULTISYNTH0_PARAMETERS_3;
      break;
  case 1:
      Rreg = Si5351_REGISTER_52_MULTISYNTH1_PARAMETERS_3;
      break;
  case 2:
      Rreg = Si5351_REGISTER_60_MULTISYNTH2_PARAMETERS_3;
      break;
  }

  read8( Config, i2c, Rreg, &regval );

  regval &= 0x0F;
  uint8_t divider = div;
  divider &= 0x07;
  divider <<= 4;
  regval |= divider;
  return write8( Config, i2c, Rreg, regval );
}

/**************************************************************************/
/*!
    @brief  Configures the Multisynth divider, which determines the
            output clock frequency based on the specified PLL input.

    @param  output    The output channel to use ( 0..2 )
    @param  pllSource	The PLL input source to use, which must be one of:
                      - Si5351_PLL_A
                      - Si5351_PLL_B
    @param  div       The integer divider for the Multisynth output.
                      If pure integer values are used, this value must
                      be one of:
                      - Si5351_MULTISYNTH_DIV_4
                      - Si5351_MULTISYNTH_DIV_6
                      - Si5351_MULTISYNTH_DIV_8
                      If fractional output is used, this value must be
                      between 8 and 900.
    @param  num       The 20-bit numerator for fractional output
                      ( 0..1,048,575 ). Set this to '0' for integer output.
    @param  denom     The 20-bit denominator for fractional output
                      ( 1..1,048,575 ). Set this to '1' or higher to
                      avoid divide by zero errors.

    @section Output Clock Configuration

    The multisynth dividers are applied to the specified PLL output,
    and are used to reduce the PLL output to a valid range ( 500kHz
    to 160MHz ). The relationship can be seen in this formula, where
    fVCO is the PLL output frequency and MSx is the multisynth
    divider:

        fOUT = fVCO / MSx

    Valid multisynth dividers are 4, 6, or 8 when using integers,
    or any fractional values between 8 + 1/1,048,575 and 900 + 0/1

    The following formula is used for the fractional mode divider:

        a + b / c

    a = The integer value, which must be 4, 6 or 8 in integer mode ( MSx_INT=1 )
        or 8..900 in fractional mode ( MSx_INT=0 ).
    b = The fractional numerator ( 0..1,048,575 )
    c = The fractional denominator ( 1..1,048,575 )

    @note   Try to use integers whenever possible to avoid clock jitter

    @note   For output frequencies > 150MHz, you must set the divider
            to 4 and adjust to PLL to generate the frequency ( for example
            a PLL of 640 to generate a 160MHz output clock ). This is not
            yet supported in the driver, which limits frequencies to
            500kHz .. 150MHz.

    @note   For frequencies below 500kHz ( down to 8kHz ) Rx_DIV must be
            used, but this isn't currently implemented in the driver.
*/
/**************************************************************************/
err_t Si5351_setupMultisynth( Si5351Config_t* Config, client i2c_master_if i2c,
        uint8_t     output,
        Si5351PLL_t pllSource,
        uint32_t    div,
        uint32_t    num,
        uint32_t    denom )
{
  uint32_t P1;       /* Multisynth config register P1 */
  uint32_t P2;	     /* Multisynth config register P2 */
  uint32_t P3;	     /* Multisynth config register P3 */

  /* Basic validation */
  ASSERT( (Config->Flags & initialised), ERROR_DEVICENOTINITIALISED );
  ASSERT( output < 3,                 ERROR_INVALIDPARAMETER );  /* Channel range */
  ASSERT( div > 3,                    ERROR_INVALIDPARAMETER );  /* Divider integer value */
  ASSERT( div < 901,                  ERROR_INVALIDPARAMETER );  /* Divider integer value */
  ASSERT( denom > 0,                  ERROR_INVALIDPARAMETER ); /* Avoid divide by zero */
  ASSERT( num <= 0xFFFFF,             ERROR_INVALIDPARAMETER ); /* 20-bit limit */
  ASSERT( denom <= 0xFFFFF,           ERROR_INVALIDPARAMETER ); /* 20-bit limit */

  /* Make sure the requested PLL has been initialised */
  if ( pllSource == Si5351_PLL_A )
  {
    ASSERT( (Config->Flags & plla_configured), ERROR_INVALIDPARAMETER );
  }
  else
  {
    ASSERT( (Config->Flags & pllb_configured), ERROR_INVALIDPARAMETER );
  }

  /* Output Multisynth Divider Equations
   *
   * where: a = div, b = num and c = denom
   *
   * P1 register is an 18-bit value using following formula:
   *
   * 	P1[17:0] = 128 * a + floor( 128*( b/c ) ) - 512
   *
   * P2 register is a 20-bit value using the following formula:
   *
   * 	P2[19:0] = 128 * b - c * floor( 128*( b/c ) )
   *
   * P3 register is a 20-bit value using the following formula:
   *
   * 	P3[19:0] = c
   */

  /* Set the main PLL config registers */
  if ( num == 0 )
  {
    /* Integer mode */
    P1 = 128 * div - 512;
    P2 = num;
    P3 = denom;
  }
  else
  {
    /* Fractional mode */
    P1 = ( uint32_t )( 128 * div + floor( 128 * ( ( float )num/( float )denom ) ) - 512 );
    P2 = ( uint32_t )( 128 * num - denom * floor( 128 * ( ( float )num/( float )denom ) ) );
    P3 = denom;
  }

  /* Get the appropriate starting point for the PLL registers */
  uint8_t baseaddr = 0;
  switch ( output )
  {
    case 0:
      baseaddr = Si5351_REGISTER_42_MULTISYNTH0_PARAMETERS_1;
      break;
    case 1:
      baseaddr = Si5351_REGISTER_50_MULTISYNTH1_PARAMETERS_1;
      break;
    case 2:
      baseaddr = Si5351_REGISTER_58_MULTISYNTH2_PARAMETERS_1;
      break;
  }

  /* Set the MSx config registers */
  ASSERT_STATUS( write8( Config, i2c, baseaddr,   ( P3 & 0x0000FF00 ) >> 8 ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+1, ( P3 & 0x000000FF ) ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+2, ( P1 & 0x00030000 ) >> 16 ) );	/* ToDo: Add DIVBY4 ( >150MHz ) and R0 support ( <500kHz ) later */
  ASSERT_STATUS( write8( Config, i2c, baseaddr+3, ( P1 & 0x0000FF00 ) >> 8 ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+4, ( P1 & 0x000000FF ) ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+5, ( ( P3 & 0x000F0000 ) >> 12 ) | ( ( P2 & 0x000F0000 ) >> 16 ) ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+6, ( P2 & 0x0000FF00 ) >> 8 ) );
  ASSERT_STATUS( write8( Config, i2c, baseaddr+7, ( P2 & 0x000000FF ) ) );

  /* Configure the clk control and enable the output */
  uint8_t clkControlReg = 0x0F;                             /* 8mA drive strength, MS0 as CLK0 source, Clock not inverted, powered up */
  if ( pllSource == Si5351_PLL_B ) clkControlReg |= ( 1 << 5 ); /* Uses PLLB */
  if ( num == 0 ) clkControlReg |= ( 1 << 6 );                  /* Integer mode */
  switch ( output )
  {
    case 0:
      ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_16_CLK0_CONTROL, clkControlReg ) );
      break;
    case 1:
      ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_17_CLK1_CONTROL, clkControlReg ) );
      break;
    case 2:
      ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_18_CLK2_CONTROL, clkControlReg ) );
      break;
  }

  return ERROR_NONE;
}

/**************************************************************************/
/*!
    @brief  Enables or disables all clock outputs
*/
/**************************************************************************/
err_t Si5351_enableOutputs( Si5351Config_t* Config, client i2c_master_if i2c, int enabled ) {
  /* Make sure we've called init first */
  ASSERT( (Config->Flags & initialised), ERROR_DEVICENOTINITIALISED );

  /* Enabled desired outputs ( see Register 3 ) */
  ASSERT_STATUS( write8( Config, i2c, Si5351_REGISTER_3_OUTPUT_ENABLE_CONTROL, enabled ? 0x00: 0xFF ) );

  return ERROR_NONE;
}


[[distributable]]
void Si5351_driver( server Si5351_ifx c[n], size_t n, client i2c_master_if i2c, uint8_t addr ) {
    Si5351Config_t Config;
    Si5351_construct( &Config, addr );

    while( 1 ) {
        select {
            case c[int i].init() -> err_t result:
                result = Si5351_init( &Config, i2c );
                break;

            case c[int i].setClockBuilderData() -> err_t result:
                result = Si5351_setClockBuilderData( &Config, i2c );
                break;

            case c[int i].setupPLLInt( Si5351PLL_t pll, uint8_t mult ) -> err_t result:
                result = Si5351_setupPLL( &Config, i2c, pll, mult, 0, 1 );
                break;

            case c[int i].setupPLL( Si5351PLL_t pll, uint8_t mult, uint32_t num, uint32_t denom ) -> err_t result:
                result = Si5351_setupPLL( &Config, i2c, pll, mult, num, denom );
                break;

            case c[int i].setupMultisynthInt( uint8_t output, Si5351PLL_t pllSource, Si5351MultisynthDiv_t div ) -> err_t result:
                result = Si5351_setupMultisynth( &Config, i2c, output, pllSource, div, 0, 1 );
                break;

            case c[int i].setupMultisynth( uint8_t output, Si5351PLL_t pllSource, uint32_t div, uint32_t num, uint32_t denom ) -> err_t result:
                result = Si5351_setupMultisynth( &Config, i2c, output, pllSource, div, num, denom );
                break;

            case c[int i].setupRdiv( uint8_t output, Si5351RDiv_t div ) -> err_t result:
                result = Si5351_setupRdiv( &Config, i2c, output, div );
                break;

            case c[int i].enableOutputs( int enabled ) -> err_t result:
                result = Si5351_enableOutputs( &Config, i2c, enabled );
                break;

            case c[int i].exit():
                return;
        }
    }
}
