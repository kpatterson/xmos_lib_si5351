#ifndef _Si5351_xc_h_
#define _Si5351_xc_h_
/**************************************************************************/
/*!
    @file     Si5351.xc.h
    @author   Kevin H. Patterson (kpatterson _at_ creltek _dot_ com)
    @author   based on original code by K. Townsend (Adafruit Industries)

    @section LICENSE

    Software License Agreement (BSD License)

    Copyright (C) 2017, Kevin H. Patterson
    Copyright (c) 2014, Adafruit Industries
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holders nor the
    names of its contributors may be used to endorse or promote products
    derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/**************************************************************************/

#include <Si5351_errors.h>
#include <i2c.h>
#include <stdint.h>


#define Si5351_DEFAULT_ADDRESS            (0x60) // Assumes ADDR pin = low


typedef enum {
  Si5351_PLL_A = 0,
  Si5351_PLL_B = 1
} Si5351PLL_t;

typedef enum {
  Si5351_CRYSTAL_LOAD_6PF  = (1<<6),
  Si5351_CRYSTAL_LOAD_8PF  = (2<<6),
  Si5351_CRYSTAL_LOAD_10PF = (3<<6)
} Si5351CrystalLoad_t;

typedef enum {
  Si5351_CRYSTAL_FREQ_25MHZ = (25000000),
  Si5351_CRYSTAL_FREQ_27MHZ = (27000000)
} Si5351CrystalFreq_t;

typedef enum {
  Si5351_MULTISYNTH_DIV_4  = 4,
  Si5351_MULTISYNTH_DIV_6  = 6,
  Si5351_MULTISYNTH_DIV_8  = 8
} Si5351MultisynthDiv_t;

typedef enum {
  Si5351_R_DIV_1   = 0,
  Si5351_R_DIV_2   = 1,
  Si5351_R_DIV_4   = 2,
  Si5351_R_DIV_8   = 3,
  Si5351_R_DIV_16  = 4,
  Si5351_R_DIV_32  = 5,
  Si5351_R_DIV_64  = 6,
  Si5351_R_DIV_128 = 7,
} Si5351RDiv_t;


typedef interface Si5351_ifx {
    err_t init();

    err_t setClockBuilderData();

    err_t setupPLLInt( Si5351PLL_t pll, uint8_t mult );

    err_t setupPLL( Si5351PLL_t pll, uint8_t mult, uint32_t num, uint32_t denom );

    err_t setupMultisynthInt( uint8_t output, Si5351PLL_t pllSource, Si5351MultisynthDiv_t div );

    err_t setupMultisynth( uint8_t output, Si5351PLL_t pllSource, uint32_t div, uint32_t num, uint32_t denom );

    err_t setupRdiv( uint8_t output, Si5351RDiv_t div );

    err_t enableOutputs( int enabled );

    void exit();

} Si5351_ifx;


[[distributable]]
void Si5351_driver( server Si5351_ifx i[n], size_t n, client i2c_master_if i2c, uint8_t i2cAddr );


#endif
